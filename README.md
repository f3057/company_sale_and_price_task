# Company sale and price task

## Task description
TODO:

## Project structure description

* [`data`](data): contains actual data (customers.tsv, sales.tsv and prediction.tsv with current results)
* [`research`](research): contains python modules and scripts for the task solution
* [`sale_and_price_task_solving_pipeline.ipynb`](sale_and_price_task_solving_pipeline.ipynb):
shows the main stages of problem solution with some comments

## Current Problems
Solution is not stable. The current linear regression model shows worse results  
than trivial solution (all data mean volume) based on the `SMAPE` metric.

Also for some test data we get a negative volume value. Need to process such cases or return `nan`.

We can't recommend company to use these solution for this moment. Need to fine tune.

##  Ways to improve the current quality
1. Better data distribution researching (correlation, some seasonable dependencies etc)
2. Add features that describe some time dependencies: the average price
for the previous three months, the number of the month in order to probably
extract seasonal patterns, and so on
3. Iterative search for the best combination of features with successive
exclusion or addition of different combinations of features that may or may
not have a correlation with the predicted value
4. Search and process outliers
5. Different options for processing missing values
6. Investigation of series: search for multiplicative components and using
logarithms in order to remove
7. **USING CUSTOMERS GROUP DISTRIBUTION** for each `location` instead of trivial sum

## Python version
python3.8

## Run script form project directory
```bash
pip install -r requirements.txt
./research/fit_model.py --customers-data-tsv data/customers.tsv --sales-data-tsv data/sales.tsv --output-tsv /save/dir/output.tsv --log-level DEBUG
```