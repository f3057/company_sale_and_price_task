#!/usr/bin/env python3.8
"""Fit model and find optimal price for sales."""

import argparse
import os
import sys

sys.path.append(os.path.dirname(__file__))

from typing import Any, Dict

from loguru import logger

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression

import matplotlib.pyplot as plt
import seaborn as sns

from data_tools.data_manager import DataManager
from price_optimizer import PriceOptimizer


def parse_arguments() -> argparse.Namespace:
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    input_args = parser.add_argument_group('Input arguments')
    input_args.add_argument('--sales-data-tsv', type=str, required=True,
                            help='Sales dataset tsv-ile.')
    input_args.add_argument('--customers-data-tsv', type=str, required=True,
                            help='Customers dataset tsv-file.')

    output_args = parser.add_argument_group('Output arguments')
    output_args.add_argument('--output-tsv', type=str, required=True,
                             help='Tsv file for prediction storing.')

    optimization_args = parser.add_argument_group('Optimization arguments.')
    optimization_args.add_argument('--opt-grid-num', type=int, default=20,
                                   help='Number of ranges for optimal price '
                                        'searching.')
    optimization_args.add_argument('--price-limit', type=float, default=0.15,
                                   help='Price interval limit, applied to the '
                                        'the initial value.')

    verbosity_args = parser.add_argument_group('Verbosity')
    verbosity_args.add_argument('--log-level', type=str,
                                choices=['DEBUG', 'INFO', 'WARNING', 'ERROR',
                                         'CRITICAL'],
                                default='INFO',
                                help='Logger level mode.')
    args = parser.parse_args()
    return args


def calc_smape_metric(gt: np.ndarray, predict: np.ndarray):
    """Calculate SMAPE metric."""
    try:
        res = np.sum(2 * np.abs(predict - gt) / (
                np.abs(gt) + np.abs(predict))) / gt.shape[0] * 100
    except ZeroDivisionError:
        logger.warning(f'Zero division for {gt} and {predict}.')
        return None
    return res


def fit_model(data: Dict[str, Any]):
    """Fit model."""
    # Fit linear regression model and predict metrics.
    model = LinearRegression().fit(data['train_data']['X'],
                                   data['train_data']['Y'])
    predicted_y = model.predict(data['train_data']['X'])

    # Calculate metrics.
    smape = calc_smape_metric(data['train_data']['Y'], predicted_y)
    model_score = model.score(data['train_data']['X'], data['train_data']['Y'])
    logger.info(f'Model score: {model_score}')
    logger.info(f'Train ds SMAPE: {smape}')

    trivial_solution_smape = calc_smape_metric(
        data['train_data']['Y'], np.mean(data['train_data']['Y']))
    logger.info(f'Trivial solution train ds SMAPE: {trivial_solution_smape}')
    return model


def infer_on_test(data, model):
    """Infer passed model on some test data."""
    # Predict for test.
    test_res = model.predict(data['test_data']['X'])
    test_res = pd.DataFrame(data=test_res.reshape(-1, 1), columns=['volume'])

    # Extract necessary data and concat with results.
    df = data['test_data']['df'].copy()
    df[['year', 'month', 'day']] = df['year_month'].str.split('-', expand=True)
    df = df[['year', 'month', 'location', 'sku_id']]
    df['volume'] = test_res['volume']

    fig1 = sns.relplot(
        data=df, kind="line",
        x='month', y='volume', hue='location', col='sku_id', col_wrap=5)
    fig1.figure.suptitle('SKU volume predicted time series')
    plt.show()
    return df


def predict_optimal_price(data, model, price_idx, price_limit, time_limit):
    samples = data['test_data']['X']
    opt_price = np.zeros((samples.shape[0]))
    for i in range(0, samples.shape[0], time_limit):
        opt = PriceOptimizer(samples[i: i + time_limit],
                             model,
                             price_idx,
                             price_limit)
        solution = opt.optimize().x
        opt_price[i: i + time_limit] = solution
    logger.critical(opt_price)
    return opt_price


def main():
    """Application entry point."""
    args = parse_arguments()

    # Update logger.
    logger.remove()
    logger.add(sys.stderr, level=args.log_level)

    # Fit model.
    data_manager = DataManager(args.customers_data_tsv,
                               args.sales_data_tsv)

    data = data_manager.get_data()
    model = fit_model(data)
    df = infer_on_test(data, model)

    price_idx = data['features_description'].tolist().index('price')
    time_limit = data_manager.last_month_idx_cust - data_manager.last_month_idx
    optimal_price = predict_optimal_price(
        data, model, price_idx, args.price_limit, time_limit)
    df['optimal_price'] = optimal_price

    # Store info to tsv file.
    if os.path.exists(args.output_tsv):
        logger.warning(f'Output file {args.output_tsv} has already existed and '
                       f'will be rewritten.')
    os.makedirs(os.path.dirname(args.output_tsv), exist_ok=True)
    df.to_csv(args.output_tsv, sep='\t')


if __name__ == '__main__':
    main()
