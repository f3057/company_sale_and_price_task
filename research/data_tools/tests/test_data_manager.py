import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

import pytest

from data_manager import DataManager

DATA_DIR = os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..', '..', '..', 'data'))

# TODO: Add tests for datasets structures


@pytest.fixture(scope='module')
def data_files():
    customer_data_file = os.path.join(DATA_DIR, 'customers.tsv')
    sales_data_type = os.path.join(DATA_DIR, 'sales.tsv')
    return customer_data_file, sales_data_type


def test_data_loading(data_files):
    data = DataManager(data_files[0], data_files[1])
    assert data.customers_df is not None
    assert data.sales_df is not None


def test_invalid_extension_data_loading(data_files):
    customer_data_file = data_files[0].replace('tsv', 'csv')
    with pytest.raises(IOError) as exp:
        DataManager(customer_data_file, data_files[1])
    expected_err_message = f'Invalid extension for {customer_data_file}. ' \
                           f'`tsv` extension is expected.'
    assert str(exp.value) == expected_err_message


def test_invalid_input_file_type_data_loading(data_files):
    with pytest.raises(IOError) as exp:
        DataManager(data_files[0], None)
    expected_err_message = 'Input argument invalid type. `str` or `PathLike`' \
                           ' is expected.'
    assert str(exp.value) == expected_err_message
