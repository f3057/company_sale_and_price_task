"""Module for sale and price task."""
import os
from itertools import product
from typing import Any, Dict, List, Optional, Tuple, Union

from loguru import logger

import matplotlib.pyplot as plt
import seaborn as sns

import pandas as pd


class DataManager:
    """
    Class for `sale and price task` data managing.

    # TODO: Describe data.

    Include functional for:

    * datasets loading as pandas Dataframe
    * show main data statistics
    * data filtering (problem locations for sku, nan value price etc)
    * information concatenation for datasets
    * data for model training conversion
    * test data generation
    * data splitting
    * extract data for particular 'sku' and 'ira'

    """

    def __init__(self, customers_data_file: str,
                 sales_data_file: str,
                 process_data: str = True):
        """
        Data manager class init.

        - Load datasets as pandas Dataframe abd add date idx for better vis
        - Process if need:
            - Check location - ira have one-to-one relationship
            - Filter problem intervals for sku - ira
            - Process nan price values
            - Merge customers data and sales
            - Encode sales data for model fitting

        Parameters
        ----------
        customers_data_file: str
            Customers data tsv file.
        sales_data_file: str
            Sales data tsv file.
        process_data: bool
            Need to process pure data.

        """
        self.customers_df = DataManager.read_tsv(customers_data_file,
                                                 [['year', 'month']])
        self.sales_df = DataManager.read_tsv(sales_data_file,
                                             [['year', 'month']])

        # Add date idx and extract the last month index.
        # TODO: Check the same id - year + month combination.
        self.sales_df, self.last_month_idx = DataManager.add_date_idx(
            self.sales_df)
        self.customers_df, self.last_month_idx_cust = \
            DataManager.add_date_idx(self.customers_df)

        if process_data:
            # Check location - ira is one-to-one.
            assert DataManager.is_one_to_one(self.sales_df, 'ira', 'location')

            # Filter SKU-IRA by interval.
            self.sales_df, self.problem_cases = \
                self.filter_sku_by_date_interval()

            # Check price existence.
            self.sales_df = self.process_nan_price()

            # TODO: Check zero customers - zero volume.
            self.sales_df, self.problem_date_ira = self.add_customers_count(
                self.sales_df)

            # Encode.
            self.encoded_sales_df = self.encode_sales_df(self.sales_df)

    def add_customers_count(self, df: pd.core.frame.DataFrame) -> \
            Tuple[pd.core.frame.DataFrame, List[Tuple[int, str]]]:
        """Need add docstr."""
        # TODO: Update func interface.
        # TODO: Instead of sum extract vector with customers number for each
        #  group distribution.

        # Extract customers sum for each SKU-IRA
        # Generate new dataframe with ['date_idx', 'ira', 'customers_sum']
        # Correspond with data by ['date_idx', 'ira']
        customers_sum = self.customers_df.groupby(['ira', 'date_idx'])[
            'amount_of_customers'].transform(sum)
        new_ds = self.customers_df[['date_idx', 'ira']].copy()
        new_ds['customers_sum'] = customers_sum
        new_ds = new_ds.drop_duplicates(['date_idx', 'ira', 'customers_sum'])
        dss = pd.merge(df, new_ds, on=['date_idx', 'ira'], how='left')

        problem_date_ira = []
        if dss[dss['customers_sum'].isna()].size > 0:
            logger.warning('NaN customers in some region.')
            # TODO: Fix.
            problem_date_ira = dss[dss['customers_sum'].isna()][[
                    'date_idx', 'ira']].drop_duplicates().values.tolist()
            dss.dropna(subset=['customers_sum'], inplace=True)
        return dss, problem_date_ira

    @staticmethod
    def read_tsv(tsv_data_file: Union[str, os.PathLike],
                 parse_dates: Optional[List[List[str]]] = None) -> \
            pd.core.frame.DataFrame:
        """
        Read tsv dataset.

        # TODO:
        Parameters
        ----------
        tsv_data_file
        parse_dates

        Returns
        -------

        """
        if not isinstance(tsv_data_file, (str, os.PathLike)):
            raise IOError('Input argument invalid type. `str` or `PathLike` '
                          'is expected.')
        if tsv_data_file[-5:] == '.tsv':
            raise IOError(f'Invalid extension for {tsv_data_file}. '
                          f'`tsv` extension is expected.')
        if not os.path.exists(tsv_data_file):
            raise IOError(f'There is no file: {tsv_data_file}')

        with open(tsv_data_file, 'r') as file:
            tsv_data = pd.read_csv(file,
                                   delimiter='\t',
                                   header=0,
                                   parse_dates=parse_dates)
        return tsv_data

    @staticmethod
    def get_df_main_info(df: pd.core.frame.DataFrame, df_name: str):
        """
        Show main df statistic.

        # TODO:
        Parameters
        ----------
        df
        df_name

        Returns
        -------

        """
        print(f'{df_name} dataframe analysis\n')
        print(f'{df_name} dataframe shape: {df.shape}\n')
        print(f'Dataframe cols types:\n{df.dtypes}\n')
        print(f'Empty entities number for each column:\n{df.isna().sum()}\n')
        print(f'Null entities number for each column:\n{df.isnull().sum()}\n')
        print(f'Statistics for each number type column:\n{df.describe()}\n')
        print('---------------------------------------------------------- \n')

    @staticmethod
    def is_one_to_one(df: pd.core.frame.DataFrame, col1: str, col2: str) \
            -> bool:
        """
        Check one to one relationship between col1 and col2.

        # TODO:
        Parameters
        ----------
        df:
        col1:
        col2

        Returns
        -------
        bool:
            Is one to one.

        """
        first = df.drop_duplicates([col1, col2]).groupby(col1)[
            col2].count().max()
        second = df.drop_duplicates([col1, col2]).groupby(col2)[
            col1].count().max()
        return first + second == 2

    @staticmethod
    def add_date_idx(input_df: pd.core.frame.DataFrame) -> \
            Tuple[pd.core.frame.DataFrame, int]:
        df = input_df.copy()
        year_month_df = df[
            'year_month'].drop_duplicates().reset_index(drop=True).to_frame()
        year_month_df['date_idx'] = list(range(1, year_month_df.size + 1))
        df = pd.merge(df, year_month_df, on=['year_month'])
        return df, year_month_df['date_idx'].size

    def vis_sales(self):
        # TODO: volume - price distribution
        # TODO: volume - customers number distribution only if existed
        # SKU volume time series
        fig1 = sns.relplot(
            data=self.sales_df, kind="line",
            x='date_idx', y='volume', hue='ira', col='sku_id', col_wrap=5)
        fig1.figure.suptitle('SKU volume time series')

        # SKU price boxplot
        plt.figure()
        fig2 = sns.boxplot(
            data=self.sales_df, y='price', x='sku_id'
        )
        fig2.figure.suptitle('SKU price boxplot')

        # SKU price time series
        plt.figure()
        fig2 = sns.relplot(
            data=self.sales_df, y='price', x='date_idx', hue='ira',
            col='sku_id', col_wrap=5
        )
        fig2.figure.suptitle('SKU price time series')

        plt.show()

    def filter_sku_by_date_interval(self) -> Tuple[pd.core.frame.DataFrame,
                                                   pd.core.frame.DataFrame]:
        # TODO: Think about data approximation.
        # TODO: Add short interval filtering.

        cols = ['sku_id', 'ira']

        def extract_df_last_month_idxs(df):
            return df.groupby(cols)['date_idx'].max().unique().tolist()

        def is_not_correct_last_month(x):
            return x.max() < self.last_month_idx

        # Filt SKU - IRA time series: remove data without the last month data.
        group = self.sales_df.groupby(['sku_id', 'ira'])
        df = group.filter(lambda x: x['date_idx'].max() == self.last_month_idx)

        logger.debug(f'Each SKU-IRA last month index before filtering: '
                     f'{extract_df_last_month_idxs(self.sales_df)}. '
                     f'Df shape: {self.sales_df.shape}')
        logger.debug(f'Each SKU-IRA last month index after filtering: '
                     f'{extract_df_last_month_idxs(df)}. '
                     f'Df shape: {df.shape}')

        # Get problem cases.
        invalid_ts = group['date_idx'].transform(is_not_correct_last_month)
        problem_cases = self.sales_df[invalid_ts.values][cols].drop_duplicates(
            cols).reset_index(drop=True).values.tolist()
        logger.debug(f'Problem SKU-IRA cases:\n {problem_cases}')
        return df, problem_cases

    def process_nan_price(self) -> pd.core.frame.DataFrame:
        # TODO: Filter cases with 0 mean price value (with all NaN in ira).
        df = self.sales_df.copy()

        # Add median column for each SKU - IRA.
        mean_price = df.groupby(['sku_id', 'ira'])['price'].transform(
            lambda x: x.mean())

        zero_mean = (mean_price < 1e-6).sum()
        assert zero_mean == 0

        # Update all nan values based on mean.
        df.loc[df['price'].isnull(), 'price'] = mean_price[df['price'].isnull()]  # noqa

        # Logger info.  # TODO: Update.
        prices_before = self.sales_df[self.sales_df['price'].isnull()].groupby(
            ['sku_id', 'ira'])['price'].sum()
        logger.debug(prices_before)

        prices_after = df[self.sales_df['price'].isnull()].groupby(
            ['sku_id', 'ira'])['price'].sum()
        logger.debug(prices_after)
        return df

    def generate_test_data(self) -> pd.core.frame.DataFrame:
        # Extract unique SKU - IRA.
        sku_ira_unique = self.sales_df[
            ['sku_id', 'ira', 'location']].drop_duplicates().values.tolist()

        # Extract sku description.
        # TODO: Check unique description for sku and no the same descriptions
        #  for different skus.
        sku_descr_fields = [
            'sku_id',
            'product_category',
            'item_per_bundle',
            'shape',
            'with_alcohol',
            'filling',
            'brand'
        ]
        sku_descr = self.sales_df[sku_descr_fields].drop_duplicates()

        # Extract the last price for SKU - IRA.
        def extract_the_last_price(x):
            x.price = float(x.price.iloc[[x.date_idx.argmax()]])
            return x

        last_price = self.sales_df.groupby(['sku_id', 'ira'])[[
            'price', 'date_idx']].apply(extract_the_last_price)
        last_price = last_price.drop(columns=['date_idx'])
        last_price['sku_id'] = self.sales_df['sku_id']
        last_price['ira'] = self.sales_df['ira']
        group_cols = ['sku_id', 'ira', 'price']
        last_price = last_price.drop_duplicates(group_cols)

        logger.debug('Last price debug output:')
        logger.debug(last_price.groupby(group_cols[:-1]).count())

        # Generate test data.
        new_date = [
            list(range(self.last_month_idx + 1, self.last_month_idx_cust + 1)),
            ['2019-07-01', '2019-08-01', '2019-09-01']
        ]
        new_date = [[date_idx, date] for date_idx, date in zip(*new_date)]

        test_data = {'sku_id': [],
                     'ira': [],
                     'date_idx': [],
                     'year_month': [],
                     'location': []}
        for d in product(sku_ira_unique, new_date):
            test_data['sku_id'].append(d[0][0])
            test_data['ira'].append(d[0][1])
            test_data['location'].append(d[0][2])
            test_data['date_idx'].append(d[1][0])
            test_data['year_month'].append(d[1][1])

        test_data = pd.DataFrame(test_data)

        # Merge with price.
        test_data = pd.merge(test_data, last_price, on=['sku_id', 'ira'])

        # Merge with unique values.
        test_data = pd.merge(test_data, sku_descr, on=['sku_id'])

        # Add volume.
        test_data['volume'] = -1
        test_data = test_data.sort_index(axis=1)
        return test_data

    @staticmethod
    def encode_and_bind(df: pd.core.frame.DataFrame,
                        categorical_features: List[str]) ->  \
            pd.core.frame.DataFrame:
        updated_df = df.copy()
        for col in categorical_features:
            dummies = pd.get_dummies(updated_df[[col]])
            updated_df = pd.concat([updated_df, dummies], axis=1)
            updated_df = updated_df.drop([col], axis=1)
        return updated_df

    @staticmethod
    def encode_sales_df(df: pd.core.frame.DataFrame) -> pd.core.frame.DataFrame:  # noqa
        # TODO: mode depending encoding for experiments - single value.
        sales_cat_cols = [
            'product_category',
            'item_per_bundle',
            'shape',
            'with_alcohol',
            'filling',
            'ira',
            'brand'
        ]
        return DataManager.encode_and_bind(df, sales_cat_cols)

    def encode_customers_df(self) -> pd.core.frame.DataFrame:
        cstmr_cat_col = ['customers_type']
        return DataManager.encode_and_bind(self.customers_df, cstmr_cat_col)

    @staticmethod
    def prepare_to_fit(df: pd.core.frame.DataFrame) -> Tuple[Dict[str, Any]]:
        # Remove data and sku.
        init_df = df.copy()
        df = df.drop(columns=['year_month', 'sku_id', 'location'])
        y = df['volume']
        x = df.drop(columns=['volume', 'date_idx'])
        sample_features = x.columns
        data = {'X': x.to_numpy(),
                'Y': y.to_numpy(),
                'df': init_df}
        return data, sample_features

    def extract_sru_and_loc(self, sku_id: str, ira: str):
        # TODO: Check not in problem cases.
        df = self.encoded_sales_df.loc[(self.sales_df['sku_id'] == sku_id) &
                                       (self.sales_df['ira'] == ira)]
        data, features = self.prepare_to_fit(df)
        return data, features

    def get_data(self):
        # Generate test data and add to self.sales.
        # Get test data and concatenate with self.sales_ds.
        test_df = self.generate_test_data()
        test_df, _ = self.add_customers_count(test_df)

        test_enc_df = DataManager.encode_sales_df(test_df)

        self.encoded_sales_df = self.encoded_sales_df.reindex(sorted(
            self.encoded_sales_df.columns), axis=1)
        test_enc_df = test_enc_df.reindex(sorted(test_enc_df.columns), axis=1)

        # Prepare data.
        train_data, sample_feature = DataManager.prepare_to_fit(
            self.encoded_sales_df)
        test_data, test_sample_features = DataManager.prepare_to_fit(
            test_enc_df)
        assert((sample_feature == test_sample_features).all())
        res = {
            'train_data': train_data,
            'test_data': test_data,
            'features_description': sample_feature
        }
        return res

