"""Module with price optimizer implementation."""

from loguru import logger

import numpy as np
from scipy.optimize import minimize


class PriceOptimizer:
    """Price optimizer."""

    def __init__(self, samples_data, model, price_idx, price_limit):
        """Price optimizer init."""
        self.price_idx = price_idx
        self.samples_data = samples_data

        # Price bounds.
        self.current_price = samples_data[:, price_idx]
        price_gap = self.current_price * price_limit
        min_pr = self.current_price - price_gap
        max_pr = self.current_price + price_gap
        self.bounds = [(i, j) for i, j in zip(min_pr, max_pr)]

        # Data and model coeff.
        self.samples_data[:, price_idx] = 0
        self.regr_coeff = model.coef_

    def inverse_revenue(self, price):
        """Function to calculate inverse revenue based on input price."""
        without_price = np.matmul(self.samples_data, self.regr_coeff)
        price_weight = price * self.regr_coeff[self.price_idx]
        inverse_revenue = 1 / np.sum((without_price + price_weight) * price)
        return inverse_revenue

    def optimize(self):
        # Run optimizer.
        logger.debug(f'Init: {self.current_price.copy()}')
        solution = minimize(self.inverse_revenue,
                            self.current_price,
                            method='BFGS',
                            bounds=self.bounds)
        logger.debug(f'Solution\n: {solution}')
        return solution
